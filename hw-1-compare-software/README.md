# GB-Ansible

lab job for GB

## Сравнение систем управления конфигурациями
<table>
  <thead>
    <tr>
      <th>Характеристика</th>
      <th>CFEngine</th>
      <th>Chef</th>
      <th>Puppet</th>
      <th>SaltStack</th>
      <th>Ansible</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Configuration Management</th>
      <td>+</td>
      <td>+</td>
      <td>+</td>
      <td>+</td>
      <td>+</td>
    </tr>
    <tr>
     <th>Network Automation</th>
     <td></td>
     <td>+</td>
     <td>+</td>
     <td></td>
     <td>+</td>
    </tr>
    <tr>
     <th>Release Management</th>
     <td></td>
     <td></td>
     <td></td>
     <td></td>
     <td>+</td>
    </tr>
    <tr>
     <th>Software Configuration Management (SCM)</th>
     <td>+</td>
     <td>+</td>
     <td>+</td>
     <td>+</td>
     <td>+</td>
    </tr>
    <tr>
     <th>DevOps</th>
     <td></td>
     <td>+</td>
     <td>+</td>
     <td>+</td>
     <td>+</td>
    </tr>
    <tr>
     <th>IT Management</th>
     <td></td>
     <td>+</td>
     <td>+</td>
     <td>+</td>
     <td></td>
    </tr>
    <tr>
     <th>Continuous Delivery</th>
     <td></td>
     <td></td>
     <td>+</td>
     <td></td>
     <td>+</td>
    </tr>
    <tr>
     <th>Continuous Integration</th>
     <td></td>
     <td></td>
     <td>+</td>
     <td></td>
     <td></td>
    </tr>
  </tbody>
</table>


 ```mermaid
 %%{init: { 'theme':'dark', 'sequence': {'useMaxWidth':false} } }%%
 sequenceDiagram
   alice ->> mark: Sent a flower
 ```