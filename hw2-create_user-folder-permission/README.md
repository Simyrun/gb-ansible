# Цель работы
Выполнить на localhost следующие задачи используюя Ansible:
- [x] Создать группу доступа `group_ansible`
- [x] Создать пользователя `ansible_pb` и добавить его в группу `group_ansible`
- [x] Создать папку `ansible_pb_folder` и сделать владельцем папки `ansible_pb_folder`
- [x] Создать файл `from_pb_ansible` в папке `ansible_pb_folder`, сделать владельцем файла `ansible_pb`

Для запуска проекта требуется выполнить:
```
mkdir hw2
cd hw2
git clone https://gitlab.com/Simyrun/gb-ansible.git .
cd hw2-create_user-folder-permission
ansible-playbook pb-hw2.yaml -c local

```
